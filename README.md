Table of contents
-----------------

* Introduction
* Schema.org types


Introduction
------------

The **Schema.org Blueprints Starter Kit: Organization** module provides
a starting point for building out an Organization's content and information
model using Schema.org.


Schema.org types
----------------

Below are the Schema.org types created by this starter kit.

- **Person** (node:Person)  
  A person (alive, dead, undead, or fictional).  
  <https://schema.org/Person>

- **Organization** (node:Organization)  
  An organization such as a school, NGO, corporation, club, etc.  
  <https://schema.org/Organization>

- **Job Posting** (node:JobPosting)  
  A listing that describes a job opening in a certain organization.  
  <https://schema.org/JobPosting>
